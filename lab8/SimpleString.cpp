#include "SimpleString.hpp"

//Include any other header files as necessary


//Define and implement the SimpleString class methods in this file…

//The 2 constructors are defined for you:

SimpleString::SimpleString() {
    this->input = "Default value";
}

SimpleString::SimpleString(std::string s) {
    this->input = s;
}

//TODO: Define the getter/setter and operator overloading methods…

void SimpleString::setString(std::string str){
  this->input = str;
}
std::string SimpleString::getString(){
  return this->input;
}

SimpleString SimpleString::operator+(const SimpleString &obj){
    SimpleString tmp = *this;
    tmp.input = this->input + obj.input;
    return tmp;
}

SimpleString& SimpleString::operator=(const SimpleString &obj){
  input = obj.input;
  return *this;
}
