/* File: rps.c */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

char* getUserChoice() {
    /* Prompt the user "Enter rock, paper, or scissors: " and return
     the string they enter */
    char* userChoice = malloc(9); //allocating for 9 bits
    printf("Enter rock, paper, or scissors: \n");
    scanf("%s", userChoice);
    return userChoice;
}

char* getComputerChoice() {
    srand (time(NULL));
    /* get a pseudo-random integer between 0 and 2 (inclusive) */
    int randChoice = rand() % 3;
    
    /* If randChoice is 0, return "rock"; if randChoice is 1,
     return "paper", and if randChoice is 2, return "scissors". */
    if(randChoice == 0){
        return "rock";
    }else if(randChoice == 1){
        return "paper";
    } else {
        return "scissors";
    }
}

char* compare(char* choice1, char* choice2)
{
    /* Implement the logic of the game here. If choice1 and choice2
     are equal, the result should be "This game is a tie."
     
     Make sure to use strcmp for string comparison.
     */
    //char* choices = ["rock", "paper", "scissors"];
    if(strcmp(choice1, choice2) == 0){
        return "This game is a tie.";
    }else if(strcmp(choice1, "rock") == 0 && strcmp(choice2, "scissors") == 0){
        return "Player wins";
    }else if(strcmp(choice1, "paper") == 0 && strcmp(choice2, "rock") == 0){
        return "Player wins";
    }else if(strcmp(choice1, "scissors") == 0 && strcmp(choice2, "paper") == 0){
        return "Player wins";
    }else {
        return "Computer Wins";
    }
}

int main(int argc, char** argv)
{
    char *userChoice = NULL, *computerChoice = NULL, *outcome = NULL;
    
    userChoice = getUserChoice();
    computerChoice = getComputerChoice();
    
    outcome = compare(userChoice, computerChoice);
    
    printf("You picked %s.\n", userChoice);
    printf("Computer picked %s.\n", computerChoice);
    printf("%s\n", outcome);
    
    return 0;
}
