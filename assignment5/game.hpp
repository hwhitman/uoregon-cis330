//Haley Whitman
#ifndef GAME_HPP_
#define GAME_HPP_

#include <vector>
#include <iostream>
#include <stdlib.h>

class Game {
private:
	int width;
	int length;

	bool isValid(int i, int j);
	bool checkOverPopulation(int i, int j, char token);
	bool checkPredator(int i, int j, char token);
	bool checkReproduction(int i, int j, char token);
	bool checkStarvation(int i, int j, char token);
	bool checkFarmerMove(int i, int j, char token);
public:
	std::vector< std::vector<char> > gameField;
	Game(int width, int length);

	void setValues(int x, int y);
	void boardSizer();
	void printBoard();
	void fillBoard();
	void step();



};


#endif

