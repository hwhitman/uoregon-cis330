//Haley Whitman

#include "game.hpp"

Game::Game(int width, int length){
	setValues(width, length);
}
//

void Game::setValues(int x, int y){
	width = x;
	length = y;
}

std::vector< std::vector<char> > gameField;

void Game::boardSizer(){
	gameField.resize(width);
	for(int i = 0; i < gameField.size(); ++i){
		gameField[i].resize(length);
	}
}

void Game::printBoard(){
	for(const std::vector<char> &i : gameField){
		for(char x : i){
			std::cout << x << ' ';
		}
		std::cout << std::endl;
	}
}

void Game::fillBoard(){
	srand(time(NULL));
	for(int i = 0; i < width; i++){
		std::vector<char> temp;
		for(int j = 0; j < length; j++){
			int tempRandom = rand() % 100;
			if(tempRandom >= 90){
				gameField[i][j] = 'W';
			} else if(tempRandom >= 60){
				gameField[i][j] = 'S';
			} else if(tempRandom >= 50) {
				gameField[i][j] = 'F';
			} else{
				gameField[i][j] = '.';
			}
		}
	}
}

void Game::step(){
	for (int i = 0; i < width; i++){
		for(int j = 0; j < length; j++){
			char token = gameField[i][j];
			bool flag = false;
			if(token == 'W' || token == 'S'){
				flag = checkOverPopulation(i, j, token);
			} if((token == 'S' || token == 'W') && !flag){
				flag = checkPredator(i, j, token);
			} if(token == '.'){
				flag = checkReproduction(i, j, token);
			} if(token == 'W' && !flag){
				flag = checkStarvation(i, j, token);
			} if(token == 'F'){
				flag = checkFarmerMove(i, j, token);
			}
		}
	}
}

bool Game::isValid(int i, int j){
	if((i >= 0 && i < width) && (j >= 0 && j < length)){
		return true;
	} else{
		return false;
	}
}

bool Game::checkOverPopulation(int i, int j, char token){
	int neighborCount = -1; //don't count itself.
	for(int x = -1; x <= 1; x++){
		for(int y = -1; y <= 1; y++){
			if(isValid(i+x, j+y)){
				if(gameField[i+x][j+y] == token){
					neighborCount++;
				}
			}
		}
	}
	if(neighborCount >3){
		gameField[i][j] = '.';
		//std::cout<< "deleting" << token << std::endl;
		return true;
	}
	return false;
}

bool Game::checkPredator(int i, int j, char token){
	int wolfCount = 0;
	int farmerCount = 0;
	for(int x = -1; x <= 1; x++){
		for(int y = -1; y <= 1; y++){
			if(isValid(i+x, j+y)){
				if(gameField[i+x][j+y] == 'W'){
					wolfCount++;
				}else if(gameField[i+x][j+y] == 'F'){
					farmerCount++;
				}
			}
		}
	}
	if(wolfCount > 0 || farmerCount > 0){
		gameField[i][j] = '.';
		//std::cout<< "deleting " << token << std::endl;
		return true;
	}
	return false;
}

bool Game::checkReproduction(int i, int j, char token){
	int sheepCount = 0;
	int wolfCount = 0;
	int farmerCount= 0;
	for(int x = -1; x <= 1; x++){
		for(int y = -1; y <= 1; y++){
			if(isValid(i+x, j+y)){
				if(gameField[i+x][j+y] == 'W'){
					wolfCount++;
				} else if(gameField[i+x][j+y] == 'S'){
					sheepCount++;
				} else if(gameField[i+x][j+y] == 'F'){
					farmerCount++;
				}
			}
		}
	}
	if(sheepCount == 2){
		gameField[i][j] = 'S';
		//std::cout<< "Reproducing sheep " << token << std::endl;
		return true;
	}else if(wolfCount == 2){
		gameField[i][j] = 'W';
		//std::cout<< "Reproducing wolf " << token << std::endl;
		return true;
	}else if(farmerCount == 2){
		gameField[i][j] = 'F';
		//std::cout<< "Reproducing farmer " << token << std::endl;
		return true;
	}
	return false;
}

bool Game::checkStarvation(int i, int j, char token){
	int sheepCount = 0;
	for(int x = -1; x <= 1; x++){
		for(int y = -1; y <= 1; y++){
			if(isValid(i+x, j+y)){
				if(gameField[i+x][j+y] == 'S'){
					sheepCount++;
				}
			}
		}
	}
	if(sheepCount > 0){
		return false;
	} else {
		gameField[i][j] = '.';
		//std::cout<< "Died due to starvation " << token << std::endl;
		return true;
	}	
}

bool Game::checkFarmerMove(int i, int j, char token){
	for(int x = -1; x <= 1; x++){
		for(int y = -1; y <= 1; y++){
			if(isValid(i+x, j+y)){
				if(gameField[i+x][j+y] == '.'){
					gameField[i][j] = '.';
					gameField[i+x][j+y] = 'F';
					//std::cout<< "Farmer moved " << token << std::endl;
					return true;
				}
			}
		}
	}
	return false;
}





