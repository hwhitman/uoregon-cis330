#include <iostream>

#include "game.hpp"

using namespace std;
int main()
{
	int width, length, steps;
	
	cout << "Please enter the size of the grid (int) (int)" << endl;
   	cin >> width; 
   	cin >> length;
   	cout << "Please enter the number of steps (int)" << endl;
   	cin >> steps;

   	Game game(width, length);

  	game.boardSizer();
  	cout << "Initializing Boardstate" << endl;
  	game.fillBoard();
  	game.printBoard();
  		for(int i = 1; i <= steps; i++){
  			game.step();
  			cout << endl << "Step: " << i << endl;
  			game.printBoard();
  }

}