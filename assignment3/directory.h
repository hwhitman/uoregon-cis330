//Haley Whitman 
//Assignment3
#ifndef DIRECTORY_H_
#define DIRECTORY_H_

#define SIZE 100


typedef struct Person {
	char name[100];
	char pNumber[20];
} Person ;

void addEntry(char* name, char* pNumber, Person* book);
void deleteEntry(int toDelete, struct Person* book);
void printDirectory(struct Person* book);


#endif /* DIRECTORY_H_ */