//Haley Whitman 
//Assignment3
#include <stdio.h>
#include <stdlib.h>

#include "directory.h"

void clearInputBuffer() {
  while ( getchar() != '\n' );
}

int main(){
	printf("Welcome to Phonebook\n");
	struct Person *phoneBook;
	phoneBook = (Person *)malloc(sizeof(Person)*SIZE);

	int userChoice = 0;
	while(userChoice != 4){

	  	printf("\nPlease choose an option:\n");
	  	printf("1. Insert a new entry. \n2. Delete an entry. \n3. Display current directory.\n4. Quit\nChoice is: ");
	  	scanf("%d", &userChoice);

	  	if(userChoice == 1){
	  		char name[100];
	  		char number[30];

	  		clearInputBuffer();
	  		printf("Option 1:\nEnter a name: \n");
	  		scanf ("%[^\n]s", name);

	  		clearInputBuffer();
	  		printf("Option 2:\nEnter a number: ");
	  		scanf ("%[^\n]s", number);

	  		addEntry(name, number, phoneBook);
	  	} else if(userChoice == 2){
	  		int toDelete;
	  		printDirectory(phoneBook);
	  		clearInputBuffer();
			printf("\nWhich one would you like to delete?\n");
			scanf("%d", &toDelete);
			deleteEntry(toDelete, phoneBook);
			toDelete = -1;

	  	} else if(userChoice == 3){
	  		clearInputBuffer();
	  		printDirectory(phoneBook);

	  	} else {
	  		if(userChoice==4){
	  			continue;
	  		}
	  		clearInputBuffer();
	  		printf("\nPlease enter 1, 2, 3, or 4.");

	  	}
	  	userChoice = 0;

	}
	
	free(phoneBook);
  	return 0;
}
