//Haley Whitman 
//Assignment3
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "directory.h"


int COUNT = 0;

void addEntry(char* name, char* pNumber, struct Person* book){

	//Reallocate memory array to twice as big as it was, copy old array over.
	if(COUNT == SIZE){
		book = (Person*) realloc(book, SIZE*2);
	}

	static struct Person personObject;

	struct Person temp;
	strcpy(temp.name, name);
	strcpy(temp.pNumber, pNumber);
	book[COUNT] = temp;
	COUNT++;

}

void deleteEntry(int toDelete, struct Person* book){
	//replace entry to be deleted with the last entry in the array.
	book[toDelete-1] = book[--COUNT];

}
void printDirectory(struct Person* book){
	for(int i = 0; i < COUNT; i++){
		printf("%d. %s, %s\n", i+1, book[i].name, book[i].pNumber);
	}
}