#include <stdlib.h>
#include <stdio.h>
#define ROW 5

void print5Triangle(){

	int triangle[ROW][15] = { //-1 acts as a flag.
		{0, -1},
		{0, 1, 2, -1},
		{0, 1, 2, 3, 4, -1},
		{0, 1, 2, 3, 4, 5, 6, -1},
		{0, 1, 2, 3, 4, 5, 6, 7, 8, -1}
	};
	int i, j=0, m = 0, whatRowAreWeOn = ROW;

	for(i = 0; i < ROW; ++i){
		for(m = 0; m < whatRowAreWeOn; m++){
			printf( "  "); //spaces for the start of the first int of the pyramid
		}
		while(triangle[i][j] != -1){
			printf("%d ",  triangle[i][j]);
			j++;
		}
		j = 0;
		whatRowAreWeOn = whatRowAreWeOn - 1;
		printf("\n");

	}

}