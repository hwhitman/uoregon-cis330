#include <stdlib.h>
#include <stdio.h>
#define COL 20

/* Allocate a triangle of height "height" (a 2-D array of int) */
void allocateNumberTriangle(const int height, int ***triangle){

	//Outline from array2d.c Boyanna Norris
	int i, j;
  	(*triangle) = (int **) malloc ( height * sizeof(int *) );
  	for (i = 0; i < height; i++) {
    	(*triangle)[i] = (int *) malloc ( COL * sizeof(int) );
  	}
}

/* Initialize the 2-D triangle array */
void initializeNumberTriangle(const int height, int **triangle){

	int i, j, number = 0, printNumber = 0;
	for(i=0; i < height; i++){
		for(j=0; j <= printNumber; j++){
			triangle[i][j] = number;
			number++;
		}
		printNumber = printNumber + 2;
		number = 0;
	}
}

/* Print a formatted triangle */
void printNumberTriangle(const int height, int **triangle){
	
	int i = 0, j=0, m = 0, whatRowAreWeOn = height, toPrint = 0;

	for(i = 0; i < height; ++i){
		for(m = 0; m < whatRowAreWeOn; m++){
			printf("  "); //spaces for the start of the first int of the pyramid
		}
		for(j=0; j <= toPrint; j++){
			printf("%d ", triangle[i][j]);
		}

		toPrint = toPrint + 2;
		j = 0;
		whatRowAreWeOn = whatRowAreWeOn - 1;
		printf("\n");
	}	
}

/* Free the memory for the 2-D triangle array */
void deallocateNumberTriangle(const int height, int **triangle){
	
	//outline from array2d.c Boyanna Norris
	int i;
	for (i = 0; i < height; ++i) 
    	free(triangle[i]);
  	free(triangle);
}