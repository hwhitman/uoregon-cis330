#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include "triangle.h"

void clearInputBuffer() {
  while ( getchar() != '\n' );
}

int main(){
	int height = -1;
	int **triangle;

	print5Triangle();

	while(height < 0 || height > 5){
		printf("\n Please enter the height of the triangle [1-5]: ");
		scanf("%d", &height);
		clearInputBuffer();
	}
	printf("\n here is what thingy: [%d]\n", isdigit(height));

	/* Allocate a triangle of height "height" (a 2-D array of int) */
	allocateNumberTriangle(height, &triangle);
	/* Initialize the 2-D triangle array */
	initializeNumberTriangle(height, triangle);
	/* Print a formatted triangle */
	printNumberTriangle(height, triangle);
	/* Free the memory for the 2-D triangle array */
	deallocateNumberTriangle(height, triangle);

	return 0;
}