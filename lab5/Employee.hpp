//Haley Whitman

#ifndef EMPLOYEE_HPP_
#define EMPLOYEE_HPP_

#include <string>

class Employee {
public:
	Employee();
	virtual ~Employee();
	virtual bool setAge( int age);
	virtual bool setFirst( std::string &text);
	virtual bool setLast( std::string &text);
	virtual int getAge( int age);
	virtual std::string getFirst( std::string &text);
	virtual std::string getLast( std::string &text);
private:
	int age;
	std::string firstName;
	std::string lastName;
};

#endif
