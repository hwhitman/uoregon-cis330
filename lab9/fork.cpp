#include <iostream>
#include <fstream>
#include <ctime>
#include <signal.h>
#include <sys/wait.h>

void printArray(int *randomArray, int nrolls){
  int sum = 0;
  for(int i = 0; i < nrolls; i++){
    sum += randomArray[i];
  }
  std::cout << "Sum is: " << sum << std::endl;
}

void fork1(int *randomArray){
  std::ofstream myfile;
  std::ifstream inFile;
  pid_t pid = fork();
  if(pid == 0){
    int sum = 0;
    myfile.open("out.txt");
    for(int j= 0; j <= 4; j++){
      sum += randomArray[j];
    }
    myfile << sum;
    myfile.close();
  } else {
    waitpid(pid, NULL, WUNTRACED);
    kill(pid, SIGCONT);
    int sum = 0;
    int prevSum = 0;
    inFile.open("out.txt");
    inFile >> prevSum;
    for(int i = 5; i < 10; i++){
      sum+= randomArray[i];
    }
    sum+= prevSum;
    std::cout << "After Child and parent sum: " << sum << std::endl;

  }
  std::cout << "End fork" << std::endl;
}

int main()
{
  srand ( time(0) );
  const int nrolls = 10;

  int randomArray [10];
  for (int i = 0; i < nrolls; ++i){

    int number = rand() % 100;
    randomArray[i] = (number);
  }
  printArray(randomArray, nrolls);

  fork1(randomArray);



}
