#include "date.hpp"

#include <iostream>

//with help from http://stackoverflow.com/questions/5891610/how-to-remove-characters-from-a-string
void parser(std::string &input, std::string::size_type len){
	char badChar = '/';
	for(int i = 0; i < len; i++){
		input.erase(remove(input.begin(), input.end(), badChar), input.end());
	}
}

DateCipher::DateCipher() : Cipher(), date("12/18/46") {

}

DateCipher::~DateCipher(){

}

std::string
DateCipher::encrypt( std::string &inputText ){
	std::string text = inputText;
	std::string::size_type len = text.length();
	DateCipher d;
	std::string key = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	d.date = "121846";
	//parser(d.date, 9);
	std::cout << d.date << std::endl;
	std::cout << d.date[0] << std::endl;

	int b = 0; //counter for date string
	for(int i = 0; i < len; i++){
		if(isalpha(text[i])){
			int shift = d.date[b] - 48;
			if(isupper(text[i])){
				text[i] = key[(text[i]-'A' + shift) % 26 + 26];//+26 to get to capital letters.
			} else {
				text[i] = key[(text[i]-'a' + shift) % 26];
			}
			b++;
		}
		if(b==6) { b=0; } //reset date counter once we reach the end of the date.
	}
	return text;
}

std::string
//for some reason couldn't use above code in this implementation.
DateCipher::decrypt( std::string &inputText ){
	std :: string text = inputText;
	std::string::size_type len = text.length();
	DateCipher d;
	std::string key = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	parser(d.date, 9);
	std::cout << d.date << std::endl;

	int b = 0; //counter for date string
	for(int i = 0; i < len; i++){
		if(isalpha(text[i])){
			int shift = d.date[b] - '0';
			if(isupper(text[i])){
				int a = text[i] -'A';
				int displacement = a - shift;
				if(displacement < 0){
					displacement = displacement + 26;
				}
				text[i] = key[displacement+26];
			} else {
				int a = text[i] - 'a';
				int displacement = a - shift;
				if(displacement < 0){
					displacement = displacement + 26;
				}
				text[i] = key[displacement];
			}
			b++;
		}
		if(b==6) { b=0; }
	}
	return text;
}
