//Haley Whitman

#include "caesar.hpp"

#include <iostream>
#include <map>

CaesarCipher::CaesarCipher() : Cipher(), rotation(2) {

}

CaesarCipher::~CaesarCipher(){

}

std::string
CaesarCipher::encrypt( std::string &inputText ){
    std::map <char, char> keyMap;
    std::string key = "abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    
    for(int i =0; i <= 51; i++){
        keyMap[key[i]] = key[i+1];
    }

    keyMap[' '] = 'a';
    keyMap['Z'] = 'A';

	std :: string text = inputText;
	std::string::size_type len = text.length();

	for (int i = 0; i < len; i++) {
        if(isalpha(text[i]) || text[i] == ' '){
            text[i] = keyMap[text[i]];
        }else{
            continue;
        }
    }
	return text;
}

std::string
CaesarCipher::decrypt( std::string &inputText ){
    std::map <char, char> keyMap;
    std::string key = "abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for(int i =1; i <= 51; i++){
        keyMap[key[i]] = key[i-1];
    }
    keyMap['z'] = 'y';
    keyMap[' '] = 'z';
    keyMap['a'] = ' ';
    keyMap['A'] = 'Z';

	std :: string text = inputText;
	std::string::size_type len = text.length();

    for (int i = 0; i < len; i++) {
        if(isalpha(text[i]) || text[i] == ' '){
            text[i] = keyMap[text[i]];
        } else{
            continue;
        }
    }

	return text;
}
