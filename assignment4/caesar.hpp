#ifndef CAESAR_HPP_
#define CAESAR_HPP_

#include <string>
#include "cipher.hpp"

class CaesarCipher : public Cipher {
public:
	CaesarCipher();
	virtual ~CaesarCipher();
	virtual std::string encrypt( std::string &text);
	virtual std::string decrypt( std::string &text);
private:
	int rotation;
};

#endif
